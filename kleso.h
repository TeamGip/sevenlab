#ifndef KLESO_H
#define KLESO_H

#include <QWidget>
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include <QPixmap>
#include <QTimer>
#include <QKeyEvent>

namespace Ui {
class Kleso;
}

class Kleso : public QWidget
{
    Q_OBJECT
    
public:
    explicit Kleso(QWidget *parent = 0);
    ~Kleso();
    
public slots:
    void onRotation();
    void onSetVysota();
    void keyPressEvent(QKeyEvent *event);
private:
    QGraphicsScene *m_scene;
    QGraphicsPixmapItem *m_grPixel;

    int rad;
    bool padenieBool;
    int padenieVysota;
    Ui::Kleso *ui;
    
    int baseMinVysota;
    int baseMaxVysota;
};

#endif // KLESO_H

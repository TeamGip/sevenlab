#include "kleso.h"
#include "ui_kleso.h"
#include <QPixmap>

Kleso::Kleso(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Kleso)
{
    ui->setupUi(this);
    
    m_scene =new QGraphicsScene(0,0, 500, 500);
    ui->graphicsView->setScene(m_scene);
    {
    QPixmap m_pix;
    m_pix.load(":/dog.png");
    m_pix =m_pix.scaled(150, 150);
    m_grPixel =new QGraphicsPixmapItem(m_pix);
    m_grPixel->setPos(250, 420);
    m_grPixel->setOffset(-1*m_pix.height()/2, -1*m_pix.width()/2);
    m_scene->addItem(m_grPixel);
    }
    
    
    QTimer *m_timer =new QTimer();
    connect(m_timer, SIGNAL(timeout()), this, SLOT(onRotation()));
    m_timer->start(5);
    
    padenieBool =true;
    padenieVysota =420;
    QTimer *timerVysoti =new QTimer();
    connect(timerVysoti, SIGNAL(timeout()),this,SLOT(onSetVysota()));
    timerVysoti->start(5);
    
    baseMinVysota =420;
    baseMaxVysota =75;
}

Kleso::~Kleso()
{
    delete ui;
}

void Kleso::onRotation()
{
    m_grPixel->setRotation(rad++);
    rad %=360;
}

void Kleso::onSetVysota()
{
    if(padenieVysota<baseMaxVysota) padenieBool =true;
    if(padenieVysota>baseMinVysota) padenieBool =false;
    if(padenieBool) padenieVysota++;
    else padenieVysota--;

    m_grPixel->setY(padenieVysota);    
}

void Kleso::keyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Backspace || padenieBool == true) padenieBool=false;
    else padenieBool =true;
}
